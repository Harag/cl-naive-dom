(in-package :cl-naive-dom.tests)

;; Feature testing for merge request !31
;; Allow emitting elements in attribute to align with the behaviour of query selectors searching in attributes

(testsuite  :elements-in-attributes
  
  (testcase :elements-in-attributes-1

            :expected '(:div :my-att ((:div :my-att-2 t)))
            ;; NOTE: Before !31, above would be
            ;; '(:div :my-att (#<ELEMENT {hashcode}>))
            
            :actual (with-current-package
                      (unwind-protect
                           (with-sexp
                             (with-dom
                               (:div :my-att
                                     (list (:div :my-att-2 t))))))))
  
  (testcase :elements-in-attributes-2
            :expected '(:div :my-att
                        ((:div :my-att-1 my-symbol-1)
                         (:div :my-att-2 my-value)
                         (:div :my-att-3 "my-string"))
                        "content")
            :actual (with-current-package
                      (unwind-protect
                           (let ((my-symbol-2 'my-value))
                             (with-sexp
                               (with-dom
                                 (:div :my-att
                                       (list
                                        (:div :my-att-1 'my-symbol-1)
                                        (:div :my-att-2 my-symbol-2)
                                        (:div :my-att-3 "my-string"))
                                       "content")))))))

  (testcase :elements-in-attributes-3
            :expected '(:div :my-att ((:div :my-att-1 my-symbol-1 "My content 1"))
                        (:div :my-att-2 ((:div :my-att-3 my-symbol-2 "My content 2"))
                         "My content 3"))
            :actual (with-current-package
                      (unwind-protect
                           (with-sexp
                             (with-dom
                               (:div :my-att
                                     (list
                                      (:div :my-att-1 'my-symbol-1
                                            "My content 1"))
                                     (:div :my-att-2 (list
                                                      (:div :my-att-3 'my-symbol-2
                                                            "My content 2"))
                                           "My content 3"))))))))
