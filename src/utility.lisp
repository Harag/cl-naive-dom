(in-package :cl-naive-dom)

(defgeneric extract (selector element)
  (:documentation "Extract from ELEMENT, any child or grand*-child selected by the SELECTOR.")
  (:method (selector (list-of-element list))
    (mapcan (lambda (element)
              (extract selector element))
            list-of-element))
  (:method (selector element)
    (let ((result '()))
      (labels ((search-element (element path)
                 (when (select selector element :path path)
                   (push element result))
                 (let ((subpath (cons element path)))
                   (dolist (child (children element))
                     (search-element child subpath)))))
        (search-element element '())
        (nreverse result)))))

(defgeneric attribute-equal-p (a b)
  (:documentation "True when A and B are equal attributes.")
  (:method (a b)
    (and (eql (key a) (key b))
         (string-equal (value a) (value b)))))

(defun set-equal (a b &key (test (function eql))  (key (function identity)))
  "True when A and B are equal sets
The elements keys are extracted with the KEY function,
and are compared with TEST."
  (and (subsetp a b :test test :key key)
       (subsetp b a :test test :key key)))

(defgeneric element-equal-p (a b)
  (:documentation "True when A and B are equal elements.")
  (:method (a b)
    (and (equalp (tag a) (tag b))
         (set-equal (attributes a) (attributes b) :test (function attribute-equal-p))
         (every (function element-equal-p) (children a) (children b)))))

(defgeneric copy-attribute (attribute)
  (:documentation "Create a new attribute instance with the same key and value as ATTRIBUTE.")
  (:method ((attribute t))
    attribute)
  (:method ((attribute cons))
    (copy-list attribute))
  (:method ((attribute attribute))
    (make-instance 'attribute
                   :key (key attribute)
                   :value (value attribute))))

(defgeneric copy-element (element)
  (:documentation "Create a new element instance with the same tag, attributes and children as ELEMENT.
The attributes and children are copied too.")
  (:method ((element t))
    element)
  (:method ((element cons))
    (copy-list element))
  (:method ((element element))
    (make-instance 'element
                   :tag (tag element)
                   :attributes (mapcar (function copy-attribute)
                                       (attributes element))
                   :children   (mapcar (function copy-element)
                                       (children element)))))

