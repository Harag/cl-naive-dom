(in-package :cl-naive-dom.abt)

(defparameter *indent-size* 2)

(defparameter *indent* 0)

(defmethod emit-element ((document-format (eql :abt)) tag element path &key (indent *indent*))
  (declare (ignore tag))
  (let ((emits '())
        (indentation (make-string (* indent *indent-size*) :initial-element #\space)))
    (format nil
            (if (plusp indent)
                (concatenate 'string
                             indentation
                             "<~(~A~)~{~@[ ~A~]~}>~%~{~A~^~%~}~{~A~^~%~}~%"
                             indentation
                             "~0@*</~(~A~)>")
                "<~(~A~)~{~@[ ~A~]~}>~%~{~A~^~%~}~{~A~^~%~}~%~0@*</~(~A~)>")

            (tag element)

            (mapcar (lambda (att)
                      (when (value att)
                        (format nil "~(~A~)=~S" (key att)
                                (if (listp (value att))
                                    (format nil "~{~A~^ ~}" (value att))
                                    (value att)))))
                    (attributes element))

            (nreverse emits)

            (progn
              (incf indent)
              (mapcar (lambda (child) (emit-dom document-format child path
                                                :indent indent))
                      (children element))))))

(defmethod emit-dom ((document-format (eql :abt)) element path &key (indent *indent*))
  (etypecase element
    (string
     (if (plusp indent)
         (format nil "~v{~A~:*~}~A" (* indent *indent-size*) (list " ") element)
         (format nil "~A" element)))
    (number
     element)
    (keyword
     element)

    (element
     (push element path)
     (emit-element document-format (tag element)  element path :indent indent))
    (list
     (format nil "~{~A~^~%~}"
             (mapcar (lambda (elm)
                       (emit-dom document-format elm path :indent indent))
                     element)))
    (t element)))

(defmacro with-abt (&body body)
  "Macro used to wrap the custom DSL code with, it parses the DSL to DOM
and then emits a string representation that uses angle brackets for tags."
  `(emit-dom :abt (cl-naive-dom:expand (with-dom ,@body)) nil))

